//
//  ModifyVCViewController.m
//  Miss Me Not
//
//  Created by Dalia on 3/28/13.
//  Copyright (c) 2013 Dalia. All rights reserved.
//

#import "CreateVC.h"


@implementation CreateVC
@synthesize name;
@synthesize type;
@synthesize pageTitle;
@synthesize stopButton;
@synthesize playButton;
@synthesize numberOfPages;
@synthesize recordPauseButton;
@synthesize numberOfCurrentPage;

@synthesize person;
@synthesize fetchedResultsController;
@synthesize managedObjectContext;

- (void)viewDidLoad
{
    [super viewDidLoad];

    [stopButton setEnabled:NO];
    [playButton setEnabled:NO];
    
    self.navigationController.navigationBar.topItem.title = @"New";
    
    // Depending the tab bar item selected we make visible/invisible certain controls on the UI
    switch(self.tabBarController.selectedIndex)
    {
        case 0:
        {
            self.numberOfCurrentPage.hidden = YES;
            self.numberOfPages.hidden = YES;
            self.pageTitle.hidden = YES;
            self.label_noAllPages.hidden = YES;
            self.label_noCurrentPage.hidden = YES;
            self.label_pageTitle.hidden = YES;
            self.type.selectedSegmentIndex = 0;
            self.type.enabled = NO;
            break;
        }
        case 1:
        {
            self.type.enabled = NO;
            self.type.selectedSegmentIndex = 1;
            break;
        }
        case 2:
        {
            self.numberOfCurrentPage.hidden = YES;
            self.numberOfPages.hidden = YES;
            self.pageTitle.hidden = YES;
            self.type.enabled = NO;
            self.label_noAllPages.hidden = YES;
            self.label_noCurrentPage.hidden = YES;
            self.label_pageTitle.hidden = YES;
            self.type.selectedSegmentIndex = 2;
            break;
        }
        default:
            //self.type.selected = YES;
            break;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)playRecord:(id)sender
{
   if (!recorder.recording){
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setCategory:AVAudioSessionCategoryPlayback error:nil];
        
        player = [[AVAudioPlayer alloc] initWithContentsOfURL:recorder.url error:nil];
        [player setDelegate:self];
        [player play];
    }
    
}

- (IBAction)stopRecord:(id)sender {
    [recorder stop];
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setActive:NO error:nil];
}

- (IBAction)record:(id)sender {
    
    if([self.name.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] init];
        [alert setMessage:@"Before recording, please enter the details."];
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    }
    else{
    // Stop the audio player before recording
    if (player.playing) {
        [player stop];
    }
    
    if (!recorder.recording) {
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setActive:YES error:nil];
        
        // Start recording
        [recorder record];
        [recordPauseButton setTitle:@"Pause" forState:UIControlStateNormal];
        
    } else {
        
        // Pause recording
        [recorder pause];
        [recordPauseButton setTitle:@"Record" forState:UIControlStateNormal];
    }
    
    [stopButton setEnabled:YES];
    [playButton setEnabled:NO];
        
        
    }
}

- (IBAction)dismissKeyboard:(id)sender {
    [name resignFirstResponder];
    [numberOfCurrentPage resignFirstResponder];
    [numberOfPages resignFirstResponder];
    [pageTitle resignFirstResponder];
}

- (IBAction)pickedNameOfRecord:(id)sender {
    
    
    // Set the audio file
    NSString *s;
    
    if(self.numberOfCurrentPage)
        s = [[NSString alloc] initWithFormat:@"%@_%@_%@_%@.m4a", self.person.name, self.name.text,[NSDate date],self.numberOfCurrentPage.text];
    else
        s = [[NSString alloc] initWithFormat:@"%@_%@_%@.m4a", self.person.name, self.name.text,[NSDate date]];
    
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],s,nil];
    
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    
    // Setup audio session
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    
    // Define the recorder setting
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    // Initiate and prepare the recorder
    recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:NULL];
    recorder.delegate = self;
    recorder.meteringEnabled = YES;
    [recorder prepareToRecord];
}


#pragma mark - AVAudioRecorderDelegate

- (void) audioRecorderDidFinishRecording:(AVAudioRecorder *)avrecorder successfully:(BOOL)flag{
    [recordPauseButton setTitle:@"Record" forState:UIControlStateNormal];
    [stopButton setEnabled:NO];
    [playButton setEnabled:YES];
}

#pragma mark - AVAudioPlayerDelegate

- (void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Done"
                                                    message: @"Finish playing the recording! Would you like to save the recording?"
                                                   delegate: self
                                          cancelButtonTitle:nil
                                          otherButtonTitles:@"YES",@"NO",@"Cancel",nil];
    [alert show];
}

#pragma mark UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0)
    {
        NSArray *recordData;
        NSString *s;
        switch(self.type.selectedSegmentIndex)
        {
            case 0:
                s = @"phone call";
                break;
            case 1:
                s= @"story";
                break;
            default:
                s=@"song";
                break;
        }
        NSDate* date = [NSDate date];
        
        if(self.type.selectedSegmentIndex == 1)
           recordData = [NSArray arrayWithObjects:date, [date dateByAddingTimeInterval: player.duration] , self.name.text, [recorder.url absoluteString] , s, [NSNumber numberWithInt:[self.numberOfPages.text intValue]],self.pageTitle.text,[NSNumber numberWithInt:[self.numberOfCurrentPage.text intValue]], nil];
        else
            recordData = recordData = [NSArray arrayWithObjects:[NSDate date], [date dateByAddingTimeInterval: player.duration] , self.name.text, [recorder.url absoluteString] , s, nil];
        
        [Recording recordingData:recordData ofType:self.type.selectedSegmentIndex andOwner:self.person inManagedObjectContext:self.managedObjectContext];
        
       [self resetFields];

    }
    
    // if no was selected
    if(buttonIndex == 1)
    {
        // delete the .m4a file
        NSError *error;
        NSFileManager *fileManager = [NSFileManager defaultManager];
        BOOL fileExists = [fileManager fileExistsAtPath:[recorder.url path]];
        if (fileExists)
        {
            BOOL success = [fileManager removeItemAtPath:[recorder.url path] error:&error];
            if (!success) NSLog(@"Error: %@", [error localizedDescription]);
        }
        [self resetFields];
    }

}

- (void) resetFields
{
    // reset all fields
    self.type.selectedSegmentIndex=-1;
    self.name.text = @"";
    self.numberOfCurrentPage.text = @"";
    self.numberOfPages.text = @"";
    self.pageTitle.text = @"";
}

- (IBAction)selectedType:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] init];
    NSString *s;
    switch(self.type.selectedSegmentIndex)
    {
        case 0:
            s = @"You choosed 'phone call' type.";
            break;
        case 1:
            s = @"You choosed 'story' type.";
            break;
        case 2:
            s = @"You choosed 'song' type.";
            break;                    
    }
    [alert setMessage:s];
    [alert addButtonWithTitle:@"Ok"];
    [alert show];
}
- (void)viewDidUnload {
    [self setLabel_noAllPages:nil];
    [self setLabel_noCurrentPage:nil];
    [self setLabel_pageTitle:nil];
    [super viewDidUnload];
}
@end

