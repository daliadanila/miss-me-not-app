//
//  ModifyVCViewController.h
//  Miss Me Not
//
//  Created by Dalia on 3/28/13.
//  Copyright (c) 2013 Dalia. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>
#import "Recording.h"
#import "Person.h"
#import "MyDocumentHandler.h"

@interface CreateVC: UIViewController <AVAudioRecorderDelegate, AVAudioPlayerDelegate, UIAlertViewDelegate>
{
    AVAudioRecorder *recorder;
    AVAudioPlayer *player;
}

@property (strong, nonatomic) Person *person;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

- (IBAction)playRecord:(id)sender;
- (IBAction)stopRecord:(id)sender;
- (IBAction)record:(id)sender;
- (IBAction)dismissKeyboard:(id)sender;
- (IBAction)pickedNameOfRecord:(id)sender;
- (IBAction)selectedType:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIButton *stopButton;
@property (weak, nonatomic) IBOutlet UIButton *recordPauseButton;
@property (weak, nonatomic) IBOutlet UITextField *name;
@property (weak, nonatomic) IBOutlet UISegmentedControl *type;
@property (weak, nonatomic) IBOutlet UITextField *numberOfPages;
@property (weak, nonatomic) IBOutlet UITextField *pageTitle;
@property (weak, nonatomic) IBOutlet UITextField *numberOfCurrentPage;

// labels
@property (weak, nonatomic) IBOutlet UILabel *label_noAllPages;
@property (weak, nonatomic) IBOutlet UILabel *label_noCurrentPage;
@property (weak, nonatomic) IBOutlet UILabel *label_pageTitle;

@end
