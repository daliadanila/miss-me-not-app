//
//  EditVC.h
//  Miss Me Not
//
//  Created by Dalia on 4/9/13.
//  Copyright (c) 2013 Dalia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Recording.h"
#import "CreateVC.h"

@interface EditVC : UIViewController

@property (strong, nonatomic) Person *person;
@property (strong, nonatomic) Recording *recording;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (strong, nonatomic) IBOutlet UITextField *name;
@property (strong, nonatomic) IBOutlet UISegmentedControl *type;

- (IBAction)saveDetails:(id)sender;
- (IBAction)dismissKeyboard:(id)sender;


@end
