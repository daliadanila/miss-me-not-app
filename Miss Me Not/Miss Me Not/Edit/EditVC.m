//
//  EditVC.m
//  Miss Me Not
//
//  Created by Dalia on 4/9/13.
//  Copyright (c) 2013 Dalia. All rights reserved.
//

#import "EditVC.h"


@implementation EditVC

@synthesize name;
@synthesize type;
@synthesize person;
@synthesize recording;
@synthesize managedObjectContext;
@synthesize fetchedResultsController;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.topItem.title = @"Edit";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:NO];
    
    self.name.text = recording.recordName;
    if([self.recording.type isEqualToString:@"phone call"])
        self.type.selectedSegmentIndex = 0;
    
    if([self.recording.type isEqualToString:@"story"])
        self.type.selectedSegmentIndex = 1;
    
    if([self.recording.type isEqualToString:@"song"])
        self.type.selectedSegmentIndex = 2;
}

- (IBAction)saveDetails:(id)sender {
    self.recording.recordName = self.name.text;
    switch (self.type.selectedSegmentIndex)
    {
        case 0:
            self.recording.type = @"phone call";
            break;
        case 1:
            self.recording.type = @"story";
            break;
        case 2:
            self.recording.type = @"song";
            break;
    }
    
    UIAlertView *alert = [[UIAlertView alloc]init];
                [alert setMessage:@"Changes made!"];
                [alert addButtonWithTitle:@"OK"];
                [alert show];
}

- (IBAction)dismissKeyboard:(id)sender {
    [self.name resignFirstResponder];
}

@end
