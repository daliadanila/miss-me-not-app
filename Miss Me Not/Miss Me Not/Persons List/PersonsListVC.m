//
//  PersonsListVC.m
//  Miss Me Not
//
//  Created by Dalia on 3/28/13.
//  Copyright (c) 2013 Dalia. All rights reserved.
//

#import "PersonsListVC.h"


@implementation PersonsListVC

@synthesize myTableView;
@synthesize typeOfViewController;
@synthesize managedObjectContext;
@synthesize fetchedResultsController;
@synthesize person;
@synthesize recordings;

- (void)viewDidLoad
{    
    if(self.typeOfViewController == 3)
    {
        UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithTitle:@"New" style:UIBarButtonItemStyleBordered target:self action:@selector(AddPerson:)];
	    [self.navigationItem setRightBarButtonItem:addButton];
    }
    
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSError *error = nil;
	if (![[self fetchedResultsController] performFetch:&error]) {
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		abort();
	}
}

- (void)viewDidAppear:(BOOL)animated
{
    
    [super viewDidAppear:animated];
    [self.myTableView reloadData];
    
}

- (void) loadView
{
    
    self.myTableView = [[UITableView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame] style:UITableViewStylePlain];
	self.myTableView.delegate = self;
	self.myTableView.dataSource = self;
	
	self.myTableView.autoresizesSubviews = YES;
    
    self.navigationItem.title = @"Person";
    self.view = self.myTableView;
}


- (IBAction) AddPerson:(id)sender
{
    AddPersonVC *addPerson = [[AddPersonVC alloc]initWithNibName:@"AddPersonVC" bundle:nil];
    addPerson.managedObjectContext = self.managedObjectContext;
    addPerson.fetchedResultsController = self.fetchedResultsController;
    [self.navigationController pushViewController:addPerson animated:NO];
}

#pragma mark -
#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    	
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger numberOfRows = 0;
	
    if ([[fetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[fetchedResultsController sections] objectAtIndex:section];
        numberOfRows = [sectionInfo numberOfObjects];
    }
    
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *PersonsCellIdentifier = @"PersonsCell";
    PersonsListCustomCell *cell = (PersonsListCustomCell*)[tableView dequeueReusableCellWithIdentifier:PersonsCellIdentifier];
    
    if (cell == nil) {
        [[NSBundle mainBundle] loadNibNamed:@"PersonsListCustomCell" owner:self options:nil];
        cell = self.customCell;
		
    }
    
    self.person = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.name.text = [self.person valueForKey:@"name"];
    cell.imageView.image = [UIImage imageWithContentsOfFile:[self.person valueForKey:@"imagePath"]] ;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
     self.person = [self.fetchedResultsController objectAtIndexPath:indexPath];
    Recording *record = (Recording *)[fetchedResultsController objectAtIndexPath:indexPath];
    [self showRecording:record animated:YES];
}

- (void)showRecording:(Recording *)record animated:(BOOL)animated {
    // Create a detail view controller, set the recipe, then push it.
    RecordingsVC *recordingsViewController = [[RecordingsVC alloc] initWithNibName:@"RecordingsVC" bundle:nil];
    recordingsViewController.managedObjectContext = self.managedObjectContext;
    recordingsViewController.person = self.person;
    recordingsViewController.recording = record;
    recordingsViewController.typeOfViewController = typeOfViewController;
    [self.navigationController pushViewController:recordingsViewController animated:animated];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (NSFetchedResultsController *)fetchedResultsController {
    // Set up the fetched results controller if needed.
    
    if (fetchedResultsController == nil) {
        // Create the fetch request for the entity.
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        // Edit the entity name as appropriate.
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Person" inManagedObjectContext:self.managedObjectContext];
        [fetchRequest setEntity:entity];
        
        // Edit the sort key as appropriate.
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
        [fetchRequest setSortDescriptors:sortDescriptors];
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
        aFetchedResultsController.delegate = self;
        self.fetchedResultsController = aFetchedResultsController;
        
    }
	
	return fetchedResultsController;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {

    NSManagedObjectContext *context = [fetchedResultsController managedObjectContext];
    
    if(self.typeOfViewController == 3)
    {
        if (editingStyle == UITableViewCellEditingStyleDelete) {
            
            // deleting all the files of the person we want to delete
            NSFileManager *fileManager = [NSFileManager defaultManager];
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSArray *allFiles = [fileManager contentsOfDirectoryAtURL:[NSURL URLWithString:[documentsDirectory stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] includingPropertiesForKeys:nil options:NSDirectoryEnumerationSkipsHiddenFiles error:nil];
            
            NSError *error;
            for(NSURL *file in allFiles)
            {
                if([[file absoluteString] rangeOfString:[NSString stringWithFormat: @"/%@",self.person.name]].location != NSNotFound)
                {
                    BOOL success = [fileManager removeItemAtPath:[file path] error:&error];
                    if (!success) NSLog(@"Error: %@", [error localizedDescription]);
                }
            }
            
            [context deleteObject:[fetchedResultsController objectAtIndexPath:indexPath]];
     
        }
            
    
    else if (editingStyle == UITableViewCellEditingStyleInsert)
	{
        [context insertObject:[fetchedResultsController objectAtIndexPath:indexPath]];
    }
      
	}

}

/**
 Delegate methods of NSFetchedResultsController to respond to additions, removals and so on.
 */

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
	// The fetch controller is about to start sending change notifications, so prepare the table view for updates.
	[self.myTableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
	UITableView *tableView = self.myTableView;
	
	switch(type) {
		case NSFetchedResultsChangeInsert:
			[tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeDelete:
			[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeUpdate:
			[tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeMove:
			[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
	}
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
	switch(type) {
		case NSFetchedResultsChangeInsert:
			[self.myTableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeDelete:
			[self.myTableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
			break;
	}
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
	// The fetch controller has sent all current change notifications, so tell the table view to process all updates.
	[self.myTableView endUpdates];
}

@end
