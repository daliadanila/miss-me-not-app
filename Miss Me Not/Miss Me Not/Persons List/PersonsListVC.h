//
//  PersonsListVC.h
//  Miss Me Not
//
//  Created by Dalia on 3/28/13.
//  Copyright (c) 2013 Dalia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "Person.h"
#import "Recording.h"
#import "PlayRecordingVC.h"
#import "RecordingsVC.h"
#import "MyDocumentHandler.h"
#import "AppDelegate.h"
#import "AddPersonVC.h"
#import "PersonsListCustomCell.h"

@interface PersonsListVC : UIViewController <UITableViewDelegate, UITableViewDataSource,NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) Person *person;
@property (strong, nonatomic) Recording *recordings;
@property (nonatomic, assign) IBOutlet PersonsListCustomCell *customCell;

@property (nonatomic) int typeOfViewController;
@property (strong, nonatomic) UITableView *myTableView;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@end 
