//
//  PersonsListCustomCell.h
//  Miss Me Not
//
//  Created by Dalia on 4/12/13.
//  Copyright (c) 2013 Dalia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PersonsListCustomCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UIImageView *myImageView;
@end
