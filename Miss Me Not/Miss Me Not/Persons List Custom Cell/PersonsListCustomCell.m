//
//  PersonsListCustomCell.m
//  Miss Me Not
//
//  Created by Dalia on 4/12/13.
//  Copyright (c) 2013 Dalia. All rights reserved.
//

#import "PersonsListCustomCell.h"

@implementation PersonsListCustomCell
@synthesize name;
@synthesize myImageView;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.name setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin];
        self.myImageView.autoresizingMask = UIViewAutoresizingNone;
        [self.myImageView setContentMode:UIViewContentModeScaleAspectFit];
         self.myImageView.clipsToBounds = YES;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
