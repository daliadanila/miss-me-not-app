//
//  AppDelegate.h
//  Miss Me Not
//
//  Created by Dalia on 3/30/13.
//  Copyright (c) 2013 Dalia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PersonsListVC.h"
#import "RecordingsVC.h"
#import "PlayRecordingVC.h"
#import "EditVC.h"
#import "Person.h"
#import "Recording.h"
#import "MyDocumentHandler.h"

@class PersonsListVC;

@interface AppDelegate: UIResponder <UIApplicationDelegate,UITabBarControllerDelegate>

@property (strong, nonatomic) IBOutlet UIWindow *window;
@property (strong, nonatomic) IBOutlet UITabBarController *tabBarController;
@property (strong, nonatomic) IBOutlet PersonsListVC *personsListPC;
@property (strong, nonatomic) IBOutlet PersonsListVC *personsListRM;
@property (strong, nonatomic) IBOutlet PersonsListVC *personsListSM;
@property (strong, nonatomic) IBOutlet PersonsListVC *personsListProfiles;

@property (strong, nonatomic) MyDocumentHandler* document;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end
