//
//  RecordingsVC.m
//  Miss Me Not
//
//  Created by Dalia on 3/28/13.
//  Copyright (c) 2013 Dalia. All rights reserved.
//

#import "RecordingsVC.h"

@implementation RecordingsVC

@synthesize person;
@synthesize recording;
@synthesize managedObjectContext;
@synthesize fetchedResultsController;
@synthesize typeOfViewController;
@synthesize myTableView;

- (void)viewDidLoad
{    
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSError *error = nil;
	if (![[self fetchedResultsController] performFetch:&error]) {
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		abort();
	}
   
    // if the last tab is pressed or if we don't have any recordings,new button will appear on the nav bar
    if(self.typeOfViewController == 3 || self.fetchedResultsController.fetchedObjects.count == 0)
    {
        UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithTitle:@"New" style:UIBarButtonItemStyleBordered target:self action:@selector(AddRecording:)];
	    [self.navigationItem setRightBarButtonItem:addButton];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.myTableView reloadData];
}

- (void) loadView
{
    self.myTableView = [[UITableView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame] style:UITableViewStylePlain];
	self.myTableView.delegate = self;
	self.myTableView.dataSource = self;
	self.myTableView.autoresizesSubviews = YES;
    self.navigationItem.title = @"Recordings";
    self.view = self.myTableView;
}


- (IBAction) AddRecording:(id)sender
{
    CreateVC *createViewController = [[CreateVC alloc] initWithNibName:@"CreateVC" bundle:nil];
    createViewController.managedObjectContext = self.managedObjectContext;
    createViewController.person = self.person;
    [self.navigationController pushViewController:createViewController animated:NO];
}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger numberOfRows = 0;
	
    if ([[fetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[fetchedResultsController sections] objectAtIndex:section];
        numberOfRows = [sectionInfo numberOfObjects];
    }
    
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *PersonsCellIdentifier = @"RecordingsCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:PersonsCellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewRowAnimationRight reuseIdentifier:PersonsCellIdentifier] ;
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    
    self.recording = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = [self.recording valueForKey:@"recordName"];
    if(self.typeOfViewController == 1)
        cell.detailTextLabel.text = [self.recording valueForKey:@"bookTitle"];
    else
    {
        // finded out the time interval and displayed on the screen
        unsigned int unitFlags = NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *conversionInfo = [gregorian components:unitFlags fromDate:recording.date  toDate:recording.duration options:0];
        
        int hours = [conversionInfo hour];
        int minutes = [conversionInfo minute];
        int seconds = [conversionInfo second];
        
        NSString *dfs = [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes,seconds];
        
        cell.detailTextLabel.text = dfs;                                     
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   
    if(self.typeOfViewController !=3)
    {
        PlayRecordingVC *playRecordingViewController = [[PlayRecordingVC alloc] initWithNibName:@"PlayRecordingVC" bundle:nil];
        playRecordingViewController.recording = [self.fetchedResultsController objectAtIndexPath:indexPath];
        playRecordingViewController.person = self.person;
        playRecordingViewController.typeOfViewController = self.typeOfViewController;
        playRecordingViewController.managedObjectContext = self.managedObjectContext;
        playRecordingViewController.noOfPage = [[[self.fetchedResultsController objectAtIndexPath:indexPath] valueForKey:@"bookPageNumber"]intValue];
        [self.navigationController pushViewController:playRecordingViewController animated:NO];
    }
    else
    {
        EditVC *editViewController = [[EditVC alloc] initWithNibName:@"EditVC" bundle:nil];
        editViewController.person = self.person;
        editViewController.recording = [self.fetchedResultsController objectAtIndexPath:indexPath];
        editViewController.managedObjectContext = self.managedObjectContext;
        [self.navigationController pushViewController:editViewController animated:NO];
    }
    
    
}

- (NSFetchedResultsController *)fetchedResultsController {
    
    if (fetchedResultsController == nil) {
        // Create the fetch request for the entity.
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        // Edit the entity name as appropriate.
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Recording" inManagedObjectContext:self.managedObjectContext];
        //adaugat predicat si dupa person si type
        NSPredicate *predicate;
        switch(self.typeOfViewController)
        {
            case 0:
                predicate = [NSPredicate predicateWithFormat:@"person = %@ AND type=\'phone call\'",person];
                break;
            case 1:
                predicate = [NSPredicate predicateWithFormat:@"person = %@ AND type=\'story\'",person];
                break;
            case 2:
                predicate = [NSPredicate predicateWithFormat:@"person = %@ AND type=\'song\'",person];
                break;
            case 3:
                 predicate = [NSPredicate predicateWithFormat:@"person = %@",person];
                break;
        }
        
        [fetchRequest setEntity:entity];
        [fetchRequest setPredicate:predicate];
        
        // Edit the sort key as appropriate.
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"recordName" ascending:YES];
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];        
        [fetchRequest setSortDescriptors:sortDescriptors];
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
        aFetchedResultsController.delegate = self;
        self.fetchedResultsController = aFetchedResultsController;
    }
	return fetchedResultsController;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the managed object for the given index path
        if(self.typeOfViewController == 3)
        {
            NSFileManager *fileManager = [NSFileManager defaultManager];
            NSError *error;
            if([fileManager fileExistsAtPath:self.recording.recordingPath])
            {
            BOOL success = [fileManager removeItemAtPath:self.recording.recordingPath error:&error];
            if (!success) NSLog(@"Error: %@", [error localizedDescription]);
            }
            
            NSManagedObjectContext *context = [fetchedResultsController managedObjectContext];
            [context deleteObject:[fetchedResultsController objectAtIndexPath:indexPath]];
       }
	}
}

/**
 Delegate methods of NSFetchedResultsController to respond to additions, removals and so on.
 */

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
	// The fetch controller is about to start sending change notifications, so prepare the table view for updates.
	[self.myTableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
	UITableView *tableView = self.myTableView;
	
	switch(type) {
		case NSFetchedResultsChangeInsert:
			[tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeDelete:
			[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeUpdate:
			[tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeMove:
			[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
	}
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
	switch(type) {
		case NSFetchedResultsChangeInsert:
			[self.myTableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeDelete:
			[self.myTableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
			break;
	}
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
	// The fetch controller has sent all current change notifications, so tell the table view to process all updates.
	[self.myTableView endUpdates];
}

@end
