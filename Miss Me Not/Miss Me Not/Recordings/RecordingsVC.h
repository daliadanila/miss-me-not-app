//
//  RecordingsVC.h
//  Miss Me Not
//
//  Created by Dalia on 3/28/13.
//  Copyright (c) 2013 Dalia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Person.h"
#import "Recording.h"
#import "PlayRecordingVC.h"
#import "EditVC.h"

@interface RecordingsVC : UIViewController<UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) Person *person;
@property (nonatomic, strong) Recording *recording;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (nonatomic) int typeOfViewController;
@property (nonatomic,strong) UITableView *myTableView;

@end
