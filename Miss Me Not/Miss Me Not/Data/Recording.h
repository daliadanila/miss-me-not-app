//
//  Recording.h
//  Miss Me Not
//
//  Created by dalia on 4/20/13.
//  Copyright (c) 2013 Dalia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Person;

@interface Recording : NSManagedObject

@property (nonatomic, strong) NSNumber * bookNoOfPages;
@property (nonatomic, strong) NSNumber * bookPageNumber;
@property (nonatomic, strong) NSString * bookTitle;
@property (nonatomic, strong) NSDate * date;
@property (nonatomic, strong) NSDate * duration;
@property (nonatomic, strong) NSDate * phoneCallBeginTime;
@property (nonatomic, strong) NSDate * phoneCallDayOfWeek;
@property (nonatomic, strong) NSDate * phoneCallEndTime;
@property (nonatomic, strong) NSString * recordingPath;
@property (nonatomic, strong) NSString * recordName;
@property (nonatomic, strong) NSString * type;
@property (nonatomic, strong) Person *person;

+ (Recording *)recordingData:(NSArray*) recordData ofType:(int) type andOwner:(Person*) p inManagedObjectContext:(NSManagedObjectContext *)context;

@end
