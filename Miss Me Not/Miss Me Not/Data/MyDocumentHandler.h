//
//  MyDocumentHandler.h
//  Miss Me Not
//
//  Created by Dalia on 4/6/13.
//  Copyright (c) 2013 Dalia. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^OnDocumentReady) (UIManagedDocument *document);
@interface MyDocumentHandler : NSObject
@property (strong, nonatomic) UIManagedDocument *document;

+ (MyDocumentHandler *)sharedDocumentHandler;
- (void)performWithDocument:(OnDocumentReady)onDocumentReady;

@end
