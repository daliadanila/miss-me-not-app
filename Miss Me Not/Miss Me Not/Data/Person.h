//
//  Person.h
//  Miss Me Not
//
//  Created by dalia on 4/20/13.
//  Copyright (c) 2013 Dalia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Person : NSManagedObject

@property (nonatomic, strong) NSString * imagePath;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSSet *recordings;
@end

@interface Person (CoreDataGeneratedAccessors)

+ (Person *)personWithData:(NSArray*) personData inManagedObjectContext:(NSManagedObjectContext *)context;

@end
