//
//  Recording.m
//  Miss Me Not
//
//  Created by dalia on 4/20/13.
//  Copyright (c) 2013 Dalia. All rights reserved.
//

#import "Recording.h"
#import "Person.h"


@implementation Recording


@dynamic bookNoOfPages;
@dynamic bookPageNumber;
@dynamic bookTitle;
@dynamic date;
@dynamic duration;
@dynamic phoneCallBeginTime;
@dynamic phoneCallDayOfWeek;
@dynamic phoneCallEndTime;
@dynamic recordingPath;
@dynamic recordName;
@dynamic type;
@dynamic person;

+ (Recording *)recordingData:(NSArray*) recordData ofType:(int) type andOwner:(Person*) p inManagedObjectContext:(NSManagedObjectContext *)context
{
	Recording *record = nil;
	
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	request.entity = [NSEntityDescription entityForName:@"Recording" inManagedObjectContext:context];
    [request setEntity:request.entity];
    
	NSError *error = nil;
	record = [[context executeFetchRequest:request error:&error] lastObject];
    
	if (!error) {
        
        record = [NSEntityDescription insertNewObjectForEntityForName:@"Recording" inManagedObjectContext:context];
        [record setValue:[recordData objectAtIndex:0] forKey:@"date"];
        [record setValue:[recordData objectAtIndex:1] forKey:@"duration"];
        [record setValue:[recordData objectAtIndex:2] forKey:@"recordName"];
        [record setValue:[recordData objectAtIndex:3] forKey:@"recordingPath"];
        [record setValue:[recordData objectAtIndex:4] forKey:@"type"];
        record.person = p;
        if(type == 1)
        {
            [record setValue:[recordData objectAtIndex:5] forKey:@"bookNoOfPages"];
            [record setValue:[recordData objectAtIndex:6] forKey:@"bookTitle"];
            [record setValue:[recordData objectAtIndex:7] forKey:@"bookPageNumber"];
        }
        
        UIAlertView *alert = [[UIAlertView alloc]init];
        [alert setMessage:@"Recording saved!"];
        [alert addButtonWithTitle:@"OK"];
        [alert show];
	}
    
	return record;
}
@end
