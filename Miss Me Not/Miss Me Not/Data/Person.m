//
//  Person.m
//  Miss Me Not
//
//  Created by dalia on 4/20/13.
//  Copyright (c) 2013 Dalia. All rights reserved.
//

#import "Person.h"

@implementation Person

@dynamic imagePath;
@dynamic name;
@dynamic recordings;

+ (Person *)personWithData:(NSArray*) personData inManagedObjectContext:(NSManagedObjectContext *)context
{
    
    Person *person = nil;
	
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	request.entity = [NSEntityDescription entityForName:@"Person" inManagedObjectContext:context];
    [request setEntity:request.entity];
    
	NSError *error = nil;
	person = [[context executeFetchRequest:request error:&error] lastObject];
    
	if (!error) {
        person = [NSEntityDescription insertNewObjectForEntityForName:@"Person" inManagedObjectContext:context];
        [person setValue:[personData objectAtIndex:0] forKey:@"name"];
        [person setValue:[personData objectAtIndex:1] forKey:@"imagePath"];
        
        UIAlertView *alert = [[UIAlertView alloc]init];
        [alert setMessage:@"Profile saved!"];
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    }
    
	
	return person;
}
@end
