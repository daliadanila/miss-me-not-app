//
//  PlayRecordingVC.m
//  Miss Me Not
//
//  Created by Dalia on 3/28/13.
//  Copyright (c) 2013 Dalia. All rights reserved.
//

#import "PlayRecordingVC.h"


@implementation PlayRecordingVC

@synthesize person;
@synthesize recording;
@synthesize managedObjectContext;
@synthesize fetchedResultsController;
@synthesize typeOfViewController;
@synthesize myTimer;
@synthesize dateFromString;
@synthesize noOfPage;


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:NO];
    
}

-(void) viewWillDisappear:(BOOL)animated
{
    [player stop];
    [myTimer invalidate];
}
- (void)applicationDidFinishLaunching:(UIApplication *)application {
    
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
}

- (void) playRingtone
{
    NSString *urlString;
    NSError * error;
    switch(self.typeOfViewController)
    {
        case 0:
            urlString= [[NSBundle mainBundle] pathForResource:@"phone_call" ofType:@"m4a"];
            break;
        case 1:
            urlString = [[NSBundle mainBundle] pathForResource:@"story" ofType:@"mp3"];
            break;
        case 2:
            urlString = [[NSBundle mainBundle] pathForResource:@"song" ofType:@"mp3"];
            break;
            
    }
    
    NSURL *url = [[NSURL alloc] initFileURLWithPath:urlString];
    
    
    playerRingtone = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    if(!playerRingtone)
        NSLog(@"Error %@",error);
    
    [playerRingtone setDelegate:self];
    [playerRingtone prepareToPlay];
    [playerRingtone play];

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.topItem.title = @"Play";
    self.slider.minimumValue = 0;
    self.slider.maximumValue = player.duration;
    NSString *imageS;
    switch(self.typeOfViewController)
    {
        case 0: imageS = @"phone_image.png";
            break;
        case 1:
            imageS = @"story_image.png";
            break;
        case 2:
            imageS = @"song_image.png";
            break;
    }
    
    self.imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageS]];
    self.imageView.frame = CGRectMake(0,0,320,400);
    [self.view addSubview:self.imageView];
    [self playRingtone];
    
    NSError *error = nil;
	if (![[self fetchedResultsController] performFetch:&error]) {
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		abort();
	}
    
    self.pause.enabled = NO;
    self.backward.enabled = NO;
    self.forward.enabled = NO;
    
    if(self.typeOfViewController != 1)
    {
        self.previous.hidden = YES;
        self.next.hidden = YES;
    }
    
    self.recordingName.text = self.recording.recordName;
    self.titleOfCurrentStory.text = self.recording.bookTitle;
    self.date.text = [NSDateFormatter localizedStringFromDate:self.recording.date dateStyle:NSDateFormatterFullStyle timeStyle:NSDateFormatterNoStyle];
    
    // finded out the time interval and displayed on the screen
    unsigned int unitFlags = NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *conversionInfo = [gregorian components:unitFlags fromDate:recording.date  toDate:recording.duration options:0];
    
    int hours = [conversionInfo hour];
    int minutes = [conversionInfo minute];
    int seconds = [conversionInfo second];
    
    self.dateFromString = [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes,seconds];
    
    self.timeRemaining.text = [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes,seconds];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)play:(id)sender
{
    self.pause.enabled = YES;
    self.forward.enabled = YES;
    self.backward.enabled = YES;
    
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL URLWithString: recording.recordingPath] error:nil];
    [player setDelegate:self];
    [player play];
    
    self.timeRemaining.text = dateFromString;
    myTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(advanceTimerPlay:) userInfo:nil repeats:YES];
    
}

// if the user taps play or stop and then play, the timer needs to get the initial time interval
- (void)advanceTimerPlay:(NSTimer *)timer
{
   
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"HH:mm:ss"];
    NSDate * currentDate = [df dateFromString:self.timeRemaining.text];
    
    if(![currentDate isEqualToDate:[df dateFromString:@"00:00:00"]])
    {
        currentDate = [currentDate dateByAddingTimeInterval:-1];
        self.timeRemaining.text = [df stringFromDate:currentDate];
    }
    else
    {
        [UIApplication cancelPreviousPerformRequestsWithTarget:self];
    }

}

- (IBAction)stop:(id)sender
{
    [player stop];
    player.delegate = nil;
    player = nil;
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setActive:NO error:nil];
    self.timeRemaining.text = @"00:00:00";
    self.pause.enabled = NO;
    self.backward.enabled = NO;
    self.forward.enabled = NO;
    [myTimer invalidate];
}

- (void) dealloc
{
    NSLog(@"Here!");
}

- (IBAction)backward:(id)sender
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"HH:mm:ss"];
    NSDate * d = [df dateFromString:self.timeRemaining.text];
    d = [d dateByAddingTimeInterval:5];
    if([d timeIntervalSinceDate:[df dateFromString:dateFromString]]>=0)
        self.timeRemaining.text = dateFromString;
    else
        self.timeRemaining.text = [df stringFromDate:d];
    
    player.currentTime -= 5;
    [player play];
    
}

- (IBAction)forward:(id)sender
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"HH:mm:ss"];
    NSDate * d = [df dateFromString:self.timeRemaining.text];
    d = [d dateByAddingTimeInterval:-5];
    if([d timeIntervalSinceDate:[df dateFromString:@"00:00:00"]]<=0)
        self.timeRemaining.text = @"00:00:00";
    else
        self.timeRemaining.text = [df stringFromDate:d];
    
    player.currentTime += 5;
    [player play];
}

- (IBAction)previous:(id)sender
{
    [player stop];

    if(self.noOfPage > 1)
    {
        self.noOfPage--;
        self.recording = [[self.fetchedResultsController fetchedObjects] objectAtIndex:self.noOfPage-1];
    }
 
   [self viewWillAppear:YES];
    [self.myTimer invalidate];
    
}

- (IBAction)next:(id)sender
{

    [player stop];

    if(self.noOfPage < [[fetchedResultsController fetchedObjects]count])
    {
        self.noOfPage++;
        self.recording = [[self.fetchedResultsController fetchedObjects] objectAtIndex:self.noOfPage-1];
    }
    

    [self viewWillAppear:YES];
    [self.myTimer invalidate];
}

- (IBAction)pause:(id)sender
{
    if(player.playing)
    {
        [player pause];
        [self.pause setTitle:@"Resume" forState:UIControlStateNormal];
        [self.myTimer invalidate];
    }
    else
    {
        [player play];
        [self.pause setTitle:@"Pause" forState:UIControlStateNormal];
        self.myTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(advanceTimerPlay:) userInfo:nil repeats:YES];
        
    }
}


- (NSFetchedResultsController *)fetchedResultsController {
    
    if (fetchedResultsController == nil) {
        // Create the fetch request for the entity.
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        // Edit the entity name as appropriate.
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Recording" inManagedObjectContext:self.managedObjectContext];
        //adaugat predicat si dupa person si type
        NSPredicate *predicate;
        predicate = [NSPredicate predicateWithFormat:@"person = %@ AND type = \'story\' AND recordName = %@ ",person,self.recording.recordName];
      
        [fetchRequest setEntity:entity];
        [fetchRequest setPredicate:predicate];
        
        // Edit the sort key as appropriate.
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"bookPageNumber" ascending:YES];
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
        [fetchRequest setSortDescriptors:sortDescriptors];
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
        aFetchedResultsController.delegate = self;
        self.fetchedResultsController = aFetchedResultsController;
    }
	return fetchedResultsController;
}

#pragma mark - AVAudioPlayerDelegate

- (void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)playerr successfully:(BOOL)flag{
   
    if(player == playerr)
    {
        player = nil;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Done"
                                                    message: @"Finish playing the recording!"
                                                   delegate: nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
        [alert show];
        self.backward.enabled = NO;
        self.forward.enabled = NO;
        self.pause.enabled = NO;
    }
    else
    {
         [self.imageView removeFromSuperview];
    }
}

- (void)viewDidUnload {
    [self setTitleOfCurrentStory:nil];
    [self setSlider:nil];
    [super viewDidUnload];
}
- (IBAction)scrub:(UISlider *)sender {
    player.currentTime = sender.value;
}
@end
