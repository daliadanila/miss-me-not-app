//
//  PlayRecordingVC.h
//  Miss Me Not
//
//  Created by Dalia on 3/28/13.
//  Copyright (c) 2013 Dalia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "Recording.h"

@interface PlayRecordingVC : UIViewController <AVAudioPlayerDelegate,NSFetchedResultsControllerDelegate>
{
    AVAudioPlayer *player;
    AVAudioPlayer *playerRingtone;
}

@property (strong, nonatomic) UIImageView *imageView;
@property (nonatomic, strong) Recording *recording;
@property (nonatomic, strong) Person *person;

@property (nonatomic) int noOfPage;
@property (nonatomic) NSTimer *myTimer;
@property (nonatomic) NSString *dateFromString;
@property (nonatomic) int typeOfViewController;
@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (strong, nonatomic) IBOutlet UILabel *recordingName;
@property (strong, nonatomic) IBOutlet UILabel *date;
@property (strong, nonatomic) IBOutlet UILabel *duration;
@property (strong, nonatomic) IBOutlet UILabel *timeRemaining;
@property (strong, nonatomic) IBOutlet UIButton *play;
@property (strong, nonatomic) IBOutlet UIButton *pause;
@property (strong, nonatomic) IBOutlet UIButton *stop;
@property (strong, nonatomic) IBOutlet UIButton *backward;
@property (strong, nonatomic) IBOutlet UIButton *forward;
@property (strong, nonatomic) IBOutlet UIButton *previous;
@property (strong, nonatomic) IBOutlet UIButton *next;
@property (strong, nonatomic) IBOutlet UILabel *titleOfCurrentStory;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;


- (IBAction)play:(id)sender;
- (IBAction)stop:(id)sender;
- (IBAction)backward:(id)sender;
- (IBAction)forward:(id)sender;
- (IBAction)previous:(id)sender;
- (IBAction)next:(id)sender;
- (IBAction)pause:(id)sender;
- (IBAction)scrub:(UISlider *)sender;
@end
