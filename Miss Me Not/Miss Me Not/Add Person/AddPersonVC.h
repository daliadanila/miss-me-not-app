//
//  AddPersonVC.h
//  Miss Me Not
//
//  Created by Dalia on 4/12/13.
//  Copyright (c) 2013 Dalia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Person.h"
#import "MyDocumentHandler.h"
#import "PersonsListVC.h"

@interface AddPersonVC : UIViewController <UIImagePickerControllerDelegate,UIActionSheetDelegate>

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (strong, nonatomic) IBOutlet UITextField *name;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

- (IBAction)uploadPhoto:(id)sender;
- (IBAction)saveDetails:(id)sender;
- (IBAction)dismissKeyboard:(id)sender;

@end
