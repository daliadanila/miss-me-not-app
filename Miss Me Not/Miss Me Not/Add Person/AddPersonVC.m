//
//  AddPersonVC.m
//  Miss Me Not
//
//  Created by Dalia on 4/12/13.
//  Copyright (c) 2013 Dalia. All rights reserved.
//

#import "AddPersonVC.h"

@implementation AddPersonVC
@synthesize name,imageView;
@synthesize fetchedResultsController,managedObjectContext;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.topItem.title = @"New Profile";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)uploadPhoto:(id)sender
{
    UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:@"Edit profile picture" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take photo", @"Choose picture", nil];
    popupQuery.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    [popupQuery showFromTabBar:self.tabBarController.tabBar];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            [self takePhoto];
            break;
        case 1:
            [self choosePhoto];
            break;
        default:
            break;
    }
}

- (void) choosePhoto
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = (id)self;
    imagePickerController.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentModalViewController:imagePickerController animated:YES];
}

- (void)imagePickerController:(UIImagePickerController *)picker
        didFinishPickingImage:(UIImage *)image
                  editingInfo:(NSDictionary *)editingInfo
{
    
    // Dismiss the image selection, hide the picker and
    
    //show the image view with the picked image
    
    [picker dismissModalViewControllerAnimated:YES];
    self.imageView.image= image;
    
    
}

- (IBAction)dismissKeyboard:(id)sender
{
    [name resignFirstResponder];
}

- (void) takePhoto
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    
    imagePickerController.delegate = (id)self;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])  {
        imagePickerController.sourceType =  UIImagePickerControllerSourceTypeCamera;
    }
    
    [self presentModalViewController:imagePickerController animated:YES];
}

- (IBAction)saveDetails:(id)sender
{
    // verify if name is a valid string i.e. if it contains only alphanumeric characters
    NSError *error;
    NSRegularExpression *regex = [[NSRegularExpression alloc]
                                   initWithPattern:@"[a-zA-Z]" options:0 error:&error];
    if(!error)
        NSLog(@"Error %@",error);
    NSUInteger matches = [regex numberOfMatchesInString:name.text options:0
                                                  range:NSMakeRange(0, [name.text length])];
    
    
    // if it's not a valid string, the user can't save the profile
    
    //verifing if there is already a user with the same name
    BOOL exists = NO;
    for(Person * p in [self.fetchedResultsController fetchedObjects])
    {
        if([p.name isEqualToString:self.name.text])
            exists = YES;
    }
    
    if(matches<=0 || exists)
    {
        UIAlertView *alert = [[UIAlertView alloc]init];
                 [alert setMessage:@"Not a valid name/ it's already used"];
                 [alert addButtonWithTitle:@"OK"];
                 [alert show];
    }
    else
    {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,  NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* fileNameExt = [NSString stringWithFormat:@"%@.%@", self.name.text, @"png"];
    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:fileNameExt];
    UIImage *image = imageView.image; // imageView is my image from camera
    
    
    CGSize newSize = CGSizeMake(228.0, 228.0);
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    NSData *imageData = UIImagePNGRepresentation(image);
    [imageData writeToFile:savedImagePath atomically:NO];
    
    NSArray *personData = [NSArray arrayWithObjects:self.name.text,savedImagePath, nil];
    [Person personWithData:personData inManagedObjectContext:self.managedObjectContext];
    
    [self.navigationController popViewControllerAnimated:NO];
    }
}
@end
